﻿/*
6 Вариант.
Дана целочисленная матрица A{ij}i=1..n;j=1..n, n <=100. Если в матрице есть
ещё один элемент, равный её максимальному элементу, упорядочить строки матрицы
по невозрастанию количества простых чисел среди элементов строк.
*/
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>
std::ofstream out("output.txt");
void Read(int k, int m, int matrix[100][100])
{
	for (int i = 0; i < k; i++)
	{
		for (int j = 0; j < m; j++)
		{
			matrix[i][j] = 1 + rand() % 100;
		}
	}
}

void Write(int k, int m, int matrix[100][100])   // Переделать
{
	for (int i = 0; i < k; i++)
	{
		for (int j = 0; j < m; j++)
		{
			std::cout << matrix[i][j] << " ";
			out << matrix[i][j] << " ";
		}
		std::cout << std::endl;
		out << std::endl;

	}
}

bool FindMax(int k,int m,int matrix[100][100])
{
	int MaxElement = INT_MIN;
	for (int i = 0; i < k; i++)
	{
		for (int j = 0; j < m; j++)
		{
			if (matrix[i][j] >= MaxElement)
			{
				MaxElement = matrix[i][j];
			}
		}
	}
	std::cout << MaxElement << "-Наибольший элемент в матрице" << std::endl;
	out << MaxElement << "-Наибольший элемент в матрице" << std::endl;
	int count = 0;
	for (int i = 0; i < k; i++)
	{
		for (int j = 0; j < m; j++)
		{
			if (matrix[i][j] == MaxElement)
			{
				count++;
			}
		}
	}
	std::cout << count << " -Кол-во наибольших элементов в матрице" << std::endl;
	out << count << " -Кол-во наибольших элементов в матрице" << std::endl;
	if (count > 1)
	{
		return true;
	}
	return false;
}

bool isPrime(int x)
{
	if (x < 2)
	{
		return false;
	}
	for (int d = 2; d <= sqrt(x) + 1; d++)
	{
		if (x % d == 0)
		{
			return false;
		}
	}
	return true;
}

void Sort(int k,int m,int primel[100],int matrix[100][100])
{
	for (int i = 0; i < k - 1; i++)
	{
		for (int j = i + 1; j < m; j++)
		{
			if (primel[i] < primel[j])
			{
				std::swap(primel[i], primel[j]);
				std::swap(matrix[i], matrix[j]);
			}

		}
	}
}

int CountPrime(int k, int m, int matrix[100][100], int primel[100])
{
	int count = 0;
	for (int i = 0; i < k; i++)
	{
		for (int j = 0; j < m; j++)
		{
			if (isPrime(matrix[i][j]))		// считаем кол-во простых
			{
				count++;
			}
		}
		primel[i] = count;
		count = 0;
	}
	return 10;
}

int main()
{

	setlocale(LC_ALL, "Rus");
	srand(time(NULL));
	int k = 5 + rand() % 2;  //попробовать при 100 (:3)
	int m = 5 + rand() % 2;
	int matrix[100][100];
	Read(k,m,matrix);
	Write(k,m,matrix);
//	int count = 0;
	int primel[100];
	if (FindMax(k, m, matrix) == true)
	{
		std::cout << "Запускаем процесс сортировочки!" << std::endl;
		CountPrime(k, m, matrix, primel);
		for (int i = 0; i < k; i++)
		{
			std::cout << primel[i] << " - простых элементов в строке " << i+1 << std::endl;
			out << primel[i] << " - простых элементов в строке " << i+1 << std::endl;
		}
		Sort(k, m, primel, matrix);
		Write(k, m, matrix);
	} 
	return 0;
}